export class ArticleComment {
    iconId: number;
    name: string;
    text: string;
    offensive: boolean;
    thumbsUp: number;
    thumbsDown: number;
}

const comments: ArticleComment[] = [];

const commentTemplate = $("#commentTemplate").html().trim();

export const renderComments = function () {
    $('#comments').html('');

    comments.slice().reverse().forEach(comment => {
        const template = $(commentTemplate);
        template.find('img').attr('src', `https://randomuser.me/api/portraits/men/${comment.iconId}.jpg`);
        template.find('h6').text(comment.name);
        template.find('.commentText').text(comment.text);

        if (comment.offensive) {
            template.find('.thumbs').hide();
            template.find('.thumbs-blocked').show();
        } else {
            template.find('.thumbs').show();
            template.find('.thumbs-blocked').hide();

            template.find('.thumbs-down-count').text(comment.thumbsDown);
            template.find('.thumbs-up-count').text(comment.thumbsUp);
        }

        $('#comments').append(template);
    });
};

export const addComment = function (comment: ArticleComment) {
    comments.push(comment);
    renderComments();
};

addComment({
    iconId: 0,
    name: 'John Doe',
    text: 'Very nice article, you are 100% right!',
    offensive: false,
    thumbsUp: 5,
    thumbsDown: 2,
});

addComment({
    iconId: 99,
    name: 'TrollMaster99',
    text: 'U is stupid, you fucking fuck',
    offensive: true,
    thumbsUp: 0,
    thumbsDown: 0,
});

addComment({
    iconId: 1,
    name: 'Jan',
    text: 'Inspiring. Could you provide more details about how to solve problems with team engagement? It would be super helpful.',
    offensive: false,
    thumbsUp: 8,
    thumbsDown: 1,
});
