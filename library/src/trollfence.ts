import $ from 'jquery';
import {addComment, ArticleComment} from './comments';
import '../node_modules/bootstrap/js/dist/modal';
import {speakText} from './spiker';

$('form.trollfence').submit(function (event) {
    event.preventDefault();

    const text = $(this).find('input[type="text"], textarea').map(function (i, elem) {
        return $(elem).val();
    }).get().join('. ');

    fetch('https://n9u0kt8iph.execute-api.eu-central-1.amazonaws.com/v1/trollfence', {
        method: 'POST',
        body: JSON.stringify({
            text: text,
        }),
    })
        .then(res => res.json())
        .then(body => {
            const name = $(this).find('#name').val() as string;
            const text = $(this).find('#text').val() as string;

            const comment: ArticleComment = {
                iconId: Math.floor(Math.random() * 99),
                name: name,
                text: text,
                offensive: body.offensive,
                thumbsUp: Math.floor(Math.random() * 20),
                thumbsDown: Math.floor(Math.random() * 5),
            };

            const acceptComment = () => {
                console.log(comment);
                addComment(comment);
                $(this).find('input, textarea').val('');
            };

            if (comment.offensive) {
                const modal = $('#trollfenceModal');
                modal.find('#trollfenceModalName').text(name);
                modal.find('#trollfenceModalText').text(text);

                // @ts-ignore
                modal.modal();

                speakText(text);

                modal.one('hide.bs.modal', function (event) {
                    const $activeElement = $(document.activeElement);
                    if ($activeElement.is('[data-toggle], [data-dismiss]')) {
                        if (event.type === 'hide') {
                            if ($activeElement.hasClass('btn-danger')) {
                                acceptComment();
                            }
                        }
                    }
                });
            } else {
                acceptComment();
            }
        });
});
