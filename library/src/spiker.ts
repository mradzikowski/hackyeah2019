import AWS from '../node_modules/aws-sdk';

AWS.config.region = 'eu-central-1';
AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId: ''});

export function speakText(text: string) {
    const speechParams = {
        OutputFormat: "mp3",
        SampleRate: "16000",
        Text: text,
        TextType: "text",
        VoiceId: "Kimberly"
    };

    const polly = new AWS.Polly({apiVersion: '2016-06-10'});
    // @ts-ignore
    const signer = new AWS.Polly.Presigner(speechParams, polly);

    signer.getSynthesizeSpeechUrl(speechParams, function (error, url) {
        if (!error) {
            // @ts-ignore
            document.getElementById('trollfenceModalAudioSource').src = url;
            // @ts-ignore
            document.getElementById('trollfenceModalAudio').load();
            // @ts-ignore
            document.getElementById('trollfenceModalAudio').play();
        }
    });
}
