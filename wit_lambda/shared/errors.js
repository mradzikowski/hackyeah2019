const ConnectionError = {
    ERR_001: 'Connection to wit.ai api rejected or interrupted.',
    ERR_002: 'Promise cannot resolve due to http connection error.',
};

const RequestError = {
    ERR_001: 'Wrong request json, should be {"lang": "string", "text": ["string"]}',
    ERR_002: 'Language is not supported.'
}

module.exports = {
    ConnectionError: ConnectionError,
    RequestError: RequestError
}
