const fetch = require("node-fetch");
const { ConnectionError } = require("../shared/errors");

module.exports.getWitAi = async function (url, token, text) {

    const options = {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    };
    try {
        return new Promise(resolve => {
            fetch(`${url}${text}`, options)
                .then(res => res.json())
                .then(
                    data => {
                        resolve(data);
                    },
                    err => {
                        resolve(err);
                    },
                );
        });

    }
    catch (e) {
        return await Promise.reject(new Error(JSON.stringify(
            { error: ConnectionError.ERR_001 })
        ));
    }
};
