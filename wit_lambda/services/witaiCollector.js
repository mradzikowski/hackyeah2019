const { getWitAi } = require("./httpConnect");
const { calculateFromArray } = require("../helpers/sentiment");
const { ConnectionError } = require("../shared/errors");

module.exports.collectFromWitAi = async function (url, token, texts) {
    texts.forEach(text => {
        text.replace(' ', '%20');
    });
    try {
        return await Promise.resolve(calculateFromArray(
            await Promise.all(texts.map(text => getWitAi(url, token, text))))
        );
    }
    catch(_) {
        return await Promise.reject(new Error(JSON.stringify(
            { error: ConnectionError.ERR_002 })
        ));
    }
};
