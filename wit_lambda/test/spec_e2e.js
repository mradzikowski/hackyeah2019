const expect = require('chai').expect;
const witai = require('../index');

describe('Asynchronous wit.ai api connection lambda function test', () => {
    // given
    before(() => {
        process.env['url'] = 'https://api.wit.ai/message?v=20190418&q=';
        process.env['token_en'] = 'T7XDFNGEQQBK77UMXV3CB3JJCOQCHWIS';
    });
    // when
    const request_1 = {
        lang: 'en',
        parts:
            [
                'Asked by CBS News White House correspondent Paula Reid about his recognition of the presidents emotions and claims by Democrats that he is acting as a spokesperson for the White House.',
                'The statements about his beliefs, his sincere beliefs, are recognized in the report that there was substantial evidence for that," Barr added, referring to Mr. Trump. "So I m not sure what your basis is for saying that Im generous to the president',
                'In the press conference, Barr said the the White House "fully cooperated" with Muellers probe and provided "unfettered access" to documents and testimony by senior administration officials.'
            ]
    };
    const request_2 = {
        lang: 'en',
        parts:
            [
                'Get the fuck off',
                'I hate your face',
                'You should be dead'
            ]
    };
    const request_3 = {
        lang: 'en',
        parts:
            [
                'I love You',
                'It is great idea, I am proud of this.',
                'You are beautiful person, very handsome.'
            ]
    };
    it('Should to return not negative value', (done) => {
        witai.handler(request_1).then(result => {
            // then
            console.log('result looks like this: ', result);
            expect(!!result).to.be.true;
            expect(!!result['value']).to.exist;
            expect(result['value'] >= 0).to.be.true;
            done();
        });
    });
    it('Should to return negative value', (done) => {
        witai.handler(request_2).then(result => {
            // then
            console.log('result looks like this: ', result);
            expect(!!result).to.be.true;
            expect(!!result['value']).to.exist;
            expect(result['value'] < 0).to.be.true;
            done();
        });
    });
    it('Should to return positive value', (done) => {
        witai.handler(request_3).then(result => {
            // then
            console.log('result looks like this: ', result);
            expect(!!result).to.be.true;
            expect(!!result['value']).to.exist;
            expect(result['value'] > 0).to.be.true;
            done();
        });
    });
});

