const { collectFromWitAi } = require("./services/witaiCollector");
const { RequestError } = require('./shared/errors');

exports.handler = async (event) => {
    if (!!event.lang && !!event.parts && event.parts.length > 0) {
        const url = process.env.url;
        let token;
        switch (event.lang) {
            case 'en':
                token = process.env.token_en;
                break;
            case 'pl':
                token = process.env.token_pl;
                break;
            default:
                return await Promise.reject(new Error(JSON.stringify(
                    { error: RequestError.ERR_002 }
                )));
        }
        return await collectFromWitAi(url, token, event.parts);
    }
    return await Promise.reject(new Error(JSON.stringify(
        { error: RequestError.ERR_001 }
    )));
};
