module.exports.calculateFromArray = function (items) {
    const calculated = items
        .map(item => {
            if (item['entities']['sentiment']) {
                return item.entities.sentiment[0];
            }
            return { value: '', confidence: 0.0 };
        })
        .reduce((acc, sentiment) => {
            switch (sentiment.value) {
                case 'positive':
                    return acc += filterConfidenceByGradient(sentiment);
                case 'negative':
                    return acc -= filterConfidenceByGradient(sentiment);
                default:
                    return acc;
            }
        }, 0.0);
    return {
        value: !!calculated ? calculated : 0.0
    };
};

const filterConfidenceByGradient = function (value) {
    switch (true) {
        case (value.confidence >= 0.6):
            return 1.0;
        case (value.confidence >= 0.3):
            return 0.5;
        default:
            return 0.0;
    }
};
