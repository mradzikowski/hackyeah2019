import boto3, json

lambda_client = boto3.client('lambda')
lambda_splitter = "hackyeah2019-text-splitter"
lambda_witai = "hackyeah2019-witai"

def lambda_handler(event, context):
    text = event["text"]
    
    print('Calling text splitter on:', text)
    split = lambda_client.invoke(
        FunctionName=lambda_splitter,
        Payload=json.dumps(
            {
                "text": text
            }
        )
    )
    
    print('Result', split)
    
    split = json.loads(split["Payload"].read())
    print("Payload:", split.__class__, split)
    split = split["chunks"]
    
    print('Received chunks:', split)
    
    analyse = {
        "lang": "en",
        "parts": split
    }
    print('Sending them for analysis', analyse)
    # push chunks to witai for analysis
    result = lambda_client.invoke(
        FunctionName=lambda_witai,
        Payload=json.dumps(analyse)
    )
    
    result = json.loads(result["Payload"].read())
    print('Analysis result', result)
    result = result["value"]
    
    return {
        "offensive": bool(result < 0)
    }

