import json
import nltk

def spliter_handler(event, context):

    splited_text = event['text']
    to_send = []
    if len(event['text']) > 250:
        tokenizer = nltk.data.load('nltk_data/tokenizers/punkt/polish.pickle')
        res = tokenizer.tokenize(event['text'])
        for sent in res:
            if len(sent) > 249:
                splitat = 248
                l, r = sent[:splitat], sent[splitat:]
                to_send.append(l)
                to_send.append(r)
                continue
            to_send.append(sent)
    else:
        to_send.append(event['text'])
    return {
        'chunks': to_send
    }




